# Copyright 2010 Yury G. Kudryashov
# Copyright 2012 Heiko Becker
# Copyright 2013-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist="2" multibuild=false with_opt=true ]

SUMMARY="Common interface to speech synthesis"
DESCRIPTION="
Speech Dispatcher is a device independent layer for speech synthesis, developed
with the goal of making the usage of speech synthesis easier for application
programmers. It takes care of most of the tasks necessary to solve in speech-enabled
applications. What a very high level GUI library is to graphics, Speech Dispatcher
is to speech synthesis.
"
HOMEPAGE="http://devel.freebsoft.org/${PN}"
DOWNLOADS="http://www.freebsoft.org/pub/projects/${PN}/${PN/d/-dispatcher}-${PV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN/d/-dispatcher}"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    alsa [[ description = [ Use libao ALSA support for sound output ] ]]
    ao [[ description = [ Use libao for sound output ] ]]
    espeak [[ description = [ Support espeak synthesizer ] ]]
    flite [[ description = [ Support Festival Lite (flite) synthesizer ] ]]
    pulseaudio [[ description = [ Use PulseAudio for sound output ] ]]
"

# The tests need a running instance of Speech Dispatcher. cf. src_test.
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/glib:2
        dev-libs/dotconf[>=1.0]
        media-libs/libsndfile[>=1.0.2]
        alsa? ( media-libs/libao[alsa] )
        ao? ( media-libs/libao )
        espeak? ( app-speech/espeak )
        flite? ( app-speech/flite )
        pulseaudio? ( media-sound/pulseaudio )
    suggestion:
        app-speech/festival-freebsoft-utils [[
            description = [ Collection of utilities for festival needed for interaction with speechd ]
        ]]
"

WORK=${WORKBASE}/${PN/d/-dispatcher}-${PV}

src_prepare() {
    edo sed -i -e "s:env python[[:digit:].]*$:env ${PYTHON##*/}:" \
        src/api/python/{speechd/_test.py,speechd_config/spd-conf}

    edo intltoolize --force --copy --automake
}

src_configure() {
    local params=(
        --enable-nls
        --disable-static
        # ALSA audio output driver is buggy and known to cause crashes
        --without-alsa
        # We only have espeak, not -ng
        --without-espeak-ng
        # Hard-disable these backends atm
        --without-ibmtts
        --without-ivona
        # Hard-disable network audio system, needs NAS (http://radscan.com/nas.html)
        --without-nas
        --without-oss
        --without-pico
        $(option_enable python)
        $(option_with espeak)
        $(option_with flite)
        $(option_with pulseaudio pulse)
    )

    # ALSA audio output driver is buggy and known to cause crashes, thus we
    # use libao[alsa].
    if option alsa || option ao ; then
        params+=( --with-libao )
    else
        params+=( --without-libao )
    fi

    econf "${params[@]}"
}

src_test() {
    default

    # Needs a running instance of Speech Dispatcher.
    # If you care, figure out starting one properly.
    edo pushd src/tests
    edo ./run_test *.test
    edo popd
}

src_install() {
    default

    if option python ; then
        python_bytecompile
    fi
}

