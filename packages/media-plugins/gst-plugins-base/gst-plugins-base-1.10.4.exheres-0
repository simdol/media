# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gst-plugins-base test-dbus-daemon

PLATFORMS="~amd64 ~x86"
MYOPTIONS+="
    debug
    doc
    gobject-introspection
    gstreamer_plugins:
        alsa      [[ description = [ Audio input, output and mixing with ALSA ] ]]
        cdda      [[ description = [ Robust CD audio extraction using cdparanoia ] ]]
        libvisual [[ description = [ Audio visualization using libvisual ] ]]
        ogg       [[ description = [ Ogg multimedia container format support ] ]]
        opus      [[ description = [ Opus audio decoding and encoding ] ]]
        pango     [[ description = [ Pango-based text rendering and video overlaying (needed to display subtitles) ] ]]
        theora    [[ description = [ Theora video encoding and decoding using libtheora ] ]]
        vorbis    [[ description = [ Vorbis audio support using libvorbis ] ]]
        xv        [[ description = [ Video output using the XVideo extension ] ]]
"

# Tests require access to Network and X-Server
RESTRICT="test"

DEPENDENCIES+="
    build:
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.12] )
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.40.0]
        dev-libs/orc:0.4[>=0.4.24]
        media-libs/gstreamer:1.0[>=${PV}][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.31.1] )
        gstreamer_plugins:alsa? ( sys-sound/alsa-lib[>=0.9.1] )
        gstreamer_plugins:cdda? ( media/cdparanoia[>=0.10.2] )
        gstreamer_plugins:libvisual? ( media-libs/libvisual:0.4 )
        gstreamer_plugins:ogg? ( media-libs/libogg[>=1.0] )
        gstreamer_plugins:opus? ( media-libs/opus[>=0.9.4] )
        gstreamer_plugins:pango? ( x11-libs/pango[>=1.22.0] )
        gstreamer_plugins:theora? ( media-libs/libtheora[>=1.1] )
        gstreamer_plugins:vorbis? ( media-libs/libvorbis[>=1.0] )
        gstreamer_plugins:xv? ( x11-libs/libXv )
"

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    '--enable-experimental'

    # core plugins
    '--enable-adder'
    '--enable-app'
    '--enable-audioconvert'
    '--enable-audiorate'
    '--enable-audiotestsrc'
    '--enable-encoding'
    '--enable-playback'
    '--enable-audioresample'
    '--enable-subparse'
    '--enable-tcp'
    '--enable-typefind'
    '--enable-videoconvert'
    '--enable-videotestsrc'
    '--enable-videorate'
    '--enable-videoscale'
    '--enable-volume'
    '--enable-iso-codes'

    # don't compile not installed examples
    '--disable-examples'

    # already have the dependencies
    '--enable-gio'
    '--enable-orc'
    '--enable-zlib'
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=(
    'debug'
    'doc gtk-doc'
    'gobject-introspection introspection'

    # optional plugins
    'gstreamer_plugins:alsa'
    'gstreamer_plugins:cdda cdparanoia'
    'gstreamer_plugins:libvisual'
    'gstreamer_plugins:pango'
    'gstreamer_plugins:theora'
    'gstreamer_plugins:xv xvideo'
    'gstreamer_plugins:xv xshm'
    'gstreamer_plugins:ogg ogg'
    'gstreamer_plugins:opus'
    'gstreamer_plugins:vorbis vorbis'
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS+=(
    'gstreamer_plugins:xv x'
)

