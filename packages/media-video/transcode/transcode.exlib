# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'transcode-1.1.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require option-renames [ renames=[ 'v4l2 v4l' ] ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

export_exlib_phases src_prepare src_configure src_install

SUMMARY="A video stream processing tool"
DESCRIPTION="
transcode is a text console video-stream processing tool. It supports elementary
video and audio frame transformations. Some example modules are included to enable
import of MPEG-1/2, Digital Video, and other formats. It also includes export modules
for writing to AVI files with DivX, OpenDivX, XviD, Digital Video or other codecs.
Direct DVD transcoding is also supported. A set of tools is available to extract
and decode the sources into raw video/audio streams for import and to enable
post-processing of AVI files.
"
HOMEPAGE="https://bitbucket.org/france/transcode-tcforge"
DOWNLOADS="${HOMEPAGE}/downloads/${PNV}.tar.bz2"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    a52 aac alsa dv dvdread h264 imagemagick ogg quicktime sdl
    theora truetype vorbis X xvid

    lzo      [[ description = [ Enables LZO compression support ] ]]
    mjpeg    [[ description = [ Enables mjpegtools support ] ]]
    mp3      [[ description = [ Support for mp3 encoding with lame ] ]]
    v4l      [[ description = [ Enable video4linux2 support ] ]]

    ( platform: amd64 )
    ( amd64_cpu_features: 3dnow )
    ( x86_cpu_features: mmx sse sse2 )
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/libxml2:2.0
        media-libs/libmpeg2
        X? (
            x11-libs/libXaw
            x11-libs/libXext
            x11-libs/libXpm
            x11-libs/libXv
        )
        a52? ( media-libs/a52dec )
        aac? ( media-libs/faac )
        alsa? ( sys-sound/alsa-lib )
        dv? ( media-libs/libdv )
        dvdread? ( media-libs/libdvdread )
        h264? ( media-libs/x264 )
        imagemagick? ( media-gfx/ImageMagick )
        lzo? ( app-arch/lzo:2 )
        mjpeg? ( media-video/mjpegtools )
        mp3? ( media-sound/lame[>=3.93] )
        ogg? ( media-libs/libogg )
        quicktime? ( media-libs/libquicktime[>=1.0.0] )
        sdl? ( media-libs/SDL:0[>=1.2.5][X?] )
        theora? ( media-libs/libtheora )
        truetype? ( media-libs/freetype:2 )
        v4l? ( media-libs/v4l-utils )
        vorbis? ( media-libs/libvorbis )
        xvid? ( media-libs/xvid[>=1.1.3] )
        providers:ffmpeg? ( media/ffmpeg )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libav? ( media/libav )
"

transcode_src_prepare() {
    option imagemagick \
        && has_version 'media-gfx/ImageMagick[>=7]' \
        && expatch "${FILES}"/${PNV}-imagemagick7.patch

    autotools_src_prepare
}

transcode_src_configure() {
    local myconf=""

    if option amd64_cpu_features:3dnow ; then
        myconf+="--enable-3dnow --enable-mmx "
    fi

    if option platform:amd64 || option x86_cpu_features:mmx ; then
        myconf+="--enable-mmx "
    fi

    if option platform:amd64 || option x86_cpu_features:sse ; then
        myconf+="--enable-sse "
    fi

    if option platform:amd64 || option x86_cpu_features:sse2 ; then
        myconf+="--enable-sse2 "
    fi

    econf \
        --enable-ffmpeg \
        --enable-iconv \
        --enable-libavcodec \
        --enable-libavformat \
        --enable-libjpeg \
        --enable-libmpeg2 \
        --enable-libmpeg2convert \
        --enable-libxml2 \
        --enable-statbuffer \
        --disable-bktr \
        --disable-bsdav \
        --disable-ibp \
        --disable-libjpegmmx \
        --disable-nuv \
        --disable-pv3 \
        --disable-pvm3 \
        --disable-sunau \
        --disable-warnings-as-errors \
        --with-mod-path=/usr/$(exhost --target)/lib/transcode \
        $(option_enable a52) \
        $(option_enable aac faac) \
        $(option_enable alsa) \
        $(option_enable dv libdv) \
        $(option_enable dvdread libdvdread) \
        $(option_enable h264 x264) \
        $(option_enable imagemagick) \
        $(option_enable lzo) \
        $(option_enable mjpeg mjpegtools) \
        $(option_enable mp3 lame) \
        $(option_enable ogg) \
        $(option_enable 'providers:ffmpeg' libpostproc) \
        $(option_enable quicktime libquicktime) \
        $(option_enable sdl) \
        $(option_enable theora) \
        $(option_enable truetype freetype2) \
        $(option_enable v4l v4l) \
        $(option_enable v4l libv4l2) \
        $(option_enable v4l libv4lconvert) \
        $(option_enable vorbis) \
        $(option_enable xvid) \
        $(option_with X x) \
        ${myconf}
}

transcode_src_install() {
    default

    edo mv "${IMAGE}"/usr/share/doc/{${PN}/*,${PNVR}/}
    edo rmdir "${IMAGE}"/usr/share/doc/${PN}
}

