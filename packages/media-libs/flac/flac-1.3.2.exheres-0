# Copyright 2008 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.xz ]

SUMMARY="The Free Lossless Audio Codec"
HOMEPAGE+=" https://xiph.org/${PN}"

LICENCES="Xiph || ( GPL-2 GPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ogg
    ( platform: amd64 )
    ( amd64_cpu_features: avx )
    ( x86_cpu_features: avx sse )
"

# All tests are slow, so use only src_test_expensive
RESTRICT="test"

DEPENDENCIES="
    build+run:
        ogg? ( media-libs/libogg )
"

src_configure() {
    local myconf=()

    myconf+=(
        --enable-cpplibs
        --disable-doxygen-docs
        --disable-xmms-plugin
        --with-ogg-libraries=/dummy
        $(option_enable ogg)
    )

    if option platform:amd64; then
        myconf+=(
            --enable-sse
            $(option_enable amd64_cpu_features:avx)
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:avx)
            $(option_enable x86_cpu_features:sse)
        )
    fi

    econf "${myconf[@]}"
}


src_test_expensive() {
    emake check
}

