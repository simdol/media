# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Copyright 2010-2015 Wulf C: Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=SDL2_mixer-${PV}

SUMMARY="Multichannel audio mixer library for SDL"
DESCRIPTION="
SDL_mixer is a simple multi-channel audio mixer. It supports 8 channels of 16 bit
stereo audio, plus a single channel of music.
"
HOMEPAGE="https://www.libsdl.org/projects/SDL_mixer"
DOWNLOADS="${HOMEPAGE}/release/${MY_PNV}.tar.gz"

LICENCES="ZLIB"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    flac [[ description = [ Enable FLAC music ] ]]
    midi [[ description = [ Enable MIDI music via timidity ] ]]
    mikmod [[ description = [ Support mod files with libmikmod ] ]]
    ogg
    opus [[ description = [ Enable Opus music via opusfile ] ]]
    (
        mad [[ description = [ Support mp3 files with libmad ] ]]
        mpg123 [[ description = [ Support mp3 files with mpg123 ] ]]
    ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build+run:
        media-libs/SDL:2[>=2.0.7]
        flac? ( media-libs/flac )
        mad? ( media-libs/libmad )
        midi? ( media-sound/timidity++ )
        mikmod? ( media-libs/libmikmod )
        mpg123? ( media-sound/mpg123 )
        ogg? ( media-libs/libvorbis )
        opus? ( media-libs/opusfile[>=0.2] )
"

WORK=${WORKBASE}/${MY_PNV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-music-cmd
    --enable-music-midi-native
    --enable-music-wave
    --disable-music-midi-fluidsynth
    --disable-music-mod-modplug
    --disable-music-ogg-tremor
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "flac music-flac"
    "flac music-flac-shared"
    "mad music-mp3-mad-gpl"
    "mad music-mp3-mad-gpl-dithering"
    "midi music-midi"
    "midi music-midi-timidity"
    "mikmod music-mod-mikmod"
    "mikmod music-mod-mikmod-shared"
    "mpg123 music-mp3-mpg123"
    "mpg123 music-mp3-mpg123-shared"
    "ogg music-ogg"
    "ogg music-ogg-shared"
    "opus music-opus"
    "opus music-opus-shared"
)

# parallel build has a race condition
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

