# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Copyright 2010-2015 Wulf C: Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Multichannel audio mixer library for SDL"
DESCRIPTION="
SDL_mixer is a simple multi-channel audio mixer. It supports 8 channels of 16 bit
stereo audio, plus a single channel of music, mixed by the popular MikMod MOD,
Timidity MIDI, Ogg Vorbis, and SMPEG MP3 libraries.
"
HOMEPAGE="http://www.libsdl.org"
DOWNLOADS="${HOMEPAGE}/projects/${PN}/release/${PNV}.tar.gz"

BUGS_TO=""
REMOTE_IDS="freecode:${PN}"
LICENCES="LGPL-2.1"

SLOT="0"
MYOPTIONS="
    flac [[ description = [ enable FLAC music ] ]]
    midi [[ description = [ enable MIDI music via timidity ] ]]
    mikmod [[ description = [ support mod files with libmikmod ] ]]
    ogg
    (
        smpeg [[ description = [ support mp3 files with smpeg ] ]]
        mad [[ description = [ support mp3 files with libmad ] ]]
    ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build+run:
        media-libs/SDL:0[>=1.2.10]
        flac? ( media-libs/flac )
        mad? ( media-libs/libmad )
        midi? ( media-sound/timidity++ )
        mikmod? ( media-libs/libmikmod )
        ogg? ( media-libs/libvorbis )
        smpeg? ( media-libs/smpeg:0[>=0.4.3] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --disable-music-native-midi-gpl
    --disable-music-ogg-tremor
    --enable-music-cmd
    --enable-music-wave
    --enable-music-native-midi

)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "flac music-flac"
    "mad music-mp3-mad-gpl"
    "midi music-midi"
    "midi music-timidity-midi"
    "mikmod music-mod"
    "ogg music-ogg"
    "smpeg music-mp3"
)

